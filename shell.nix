let
  pkgs = import <nixpkgs> {};
  inherit (pkgs) lib stdenv;

in stdenv.mkDerivation rec {
  name = "shell";

  buildInputs = [
    pkgs.ihaskell
  ];

  installPhase = ''
    mkdir $out
    ${lib.concatMapStringsSep "\n" (i: "ln -s ${i} $out/") buildInputs}
  '';
  phases = ["installPhase"];
}
