
# Waiting Line Model

Here we are going to explore a toy simulation of a waiting line such as those cashier lines or customer service queues. Let's say it's a poor bank with one teller serving one customer queue.

## Entering the Queue

To model customers entering the queue, we are going to assume a correlation between the time one customer enters the queue and the time the next customer enters the queue. We want a function that starts at 0 and gradually goes to 1 as the interarrival time increases. Here is one such function:

![Arrival](https://gitlab.com/phunehehe/queue-simulation/-/raw/master/arrival.webp)


```haskell
-- Implement the customer arrival model, arbitrarily choosing 100 as α
arriveProbability :: Double -> Double
arriveProbability t = 1 - exp (- t / alpha)
  where alpha = 100

-- Quickly check that it returns something reasonable
arriveProbability 1
```


    9.950166250831893e-3



```haskell
arriveProbability 69
```


    0.49842393093394444



```haskell
arriveProbability 999
```


    0.9999541437933578


## Processing the Queue

The teller will take different amounts of time to serve each customer depending on their request. For each kind of request the processing time may also vary on a case by case basis.

To accommodate this we will use a few beta distributions with different parameters:

![Processing](https://gitlab.com/phunehehe/queue-simulation/-/raw/master/processing.webp)


```haskell
-- Implement the processing model for 3 customer types

data CustomerType = Blue | Red | Yellow

processingTime' :: Int -> Int -> Double -> Double
processingTime' alpha beta x = p * x ^ (alpha - 1) * (1 - x) ^ (beta - 1)
  where p = 200

processingTime :: CustomerType -> Double -> Double
processingTime Blue = processingTime' 5 1
processingTime Red = processingTime' 2 2
processingTime Yellow = processingTime' 2 5

-- Again quickly check that it returns something reasonable
processingTime Blue 0.1
```


    2.000000000000001e-2



```haskell
processingTime Red 0.1
```


    18.0



```haskell
processingTime Red 0.5
```


    50.0



```haskell
processingTime Red 0.9
```


    17.999999999999996



```haskell
processingTime Yellow 0.9
```


    1.799999999999998e-2


Now we can answer some executive questions using this model!

## Q1: For yellow customers, what are the average and maximum waiting times?

To even start answering this we need to sample the customer arrival times. We already have the formula above, but we need to do a little gymnastics to get the absolute timestamps so that we can calculate waiting times.


```haskell
-- Given an interarrival time and a list of random numbers, calculate whether a customer arrives at that time
-- If a customer arrives reset the interarrival time, otherwise increment it and continue
getArrivals :: Double -> [Double] -> [Bool]
getArrivals _ [] = []
getArrivals t (r : rs) = if r < arriveProbability t
  then True : getArrivals 1 rs
  else False : getArrivals (t + 1) rs

-- Add absolute timestamps to the list
addTimestamps :: Double -> [a] -> [(Double, a)]
addTimestamps _ [] = []
addTimestamps s (x : xs) = (s, x) : (addTimestamps (s + 1) xs)

-- Given a seed and a count, sample the requested number of seconds and return a list of timestamps at which a customer arrives
import System.Random (StdGen, randoms)
getArriveTimes :: StdGen -> Int -> [Double]
getArriveTimes g c = map fst $ filter snd $ addTimestamps 1 $ getArrivals 1 $ take c $ randoms g

-- Given a seed, a customer type and a count, sample the requested number of processing times
getProcessTimes :: StdGen -> CustomerType -> Int -> [Double]
getProcessTimes g t c = map (processingTime t) $ take c $ randoms g
```


```haskell
-- From the arrival time and the processing time, calculate when the teller starts and finishes serving each customer

getFinishTimes' :: Double -> [Double] -> [Double] -> [(Double, Double)]
getFinishTimes' _ [] [] = []
getFinishTimes' previousFinishTime (arriveTime:as) (processTime:ps) = (startTime, finishTime) : getFinishTimes' finishTime as ps
  where
    startTime = max previousFinishTime arriveTime
    finishTime = startTime + processTime

getFinishTimes :: [Double] -> [Double] -> [(Double, Double)]
getFinishTimes = getFinishTimes' 0
```


```haskell
-- From the arrival time and the start time, calculate the wait time for each customer
getWaitTimes :: [Double] -> [Double] -> [Double]
getWaitTimes [] [] = []
getWaitTimes (arriveTime:as) (startTime:ss) = (startTime - arriveTime) : getWaitTimes as ss

-- Now assuming the queue is open for a full working day of 8 hours
-- We can sample the data and calculate all the waiting times for all customers
import System.Random (mkStdGen)
arriveTimes = getArriveTimes (mkStdGen 1) $ 60 * 60 * 8
processTimes = getProcessTimes (mkStdGen 2) Yellow $ length arriveTimes
(startTimes, _finishTimes) = unzip $ getFinishTimes arriveTimes processTimes
waitTimes = getWaitTimes arriveTimes startTimes

-- Then we can calculate the average
import Data.List (genericLength)
yellowAverage = sum waitTimes / genericLength waitTimes
yellowAverage
```


    2.7635222832600843



```haskell
-- And maximum
yellowMaximum = maximum waitTimes
yellowMaximum
```


    46.89131211337008


## Q2: For red customers, what are the average and maximum queue lengths?

To answer this we need to determine the queue length at any given time. We could average the queue length of each second in the simulation, but because processing time can come in fractions we'll lose some accuracy. We cheated when sampling customer arrival times at the start of each second :)

Luckily doing the precise calculation is not hard: we just need to weight each "segment" of the imaginary queue length chart by the length of time that it occupies.


```haskell
-- Determine the segments by looking at the arrival times and the processing start times.
-- Everytime a customer arrives we increase the queue length and note down the time
-- Everytime the teller starts serving a customer we decrease the length and also note down the time

getQueueLengths' :: Double -> [Double] -> [Double] -> [(Double, Double)]
getQueueLengths' _ [] [] = []
getQueueLengths' l [] (s:ss) = (s, l - 1) : getQueueLengths' (l - 1) [] ss
getQueueLengths' l (a:as) (s:ss)
  | a == s = getQueueLengths' l as ss
  | a < s = (a, l + 1) : getQueueLengths' (l + 1) as (s:ss)
  | a > s = (s, l - 1) : getQueueLengths' (l - 1) (a:as) ss

getQueueLengths :: [Double] -> [Double] -> [(Double, Double)]
getQueueLengths = getQueueLengths' 0
```


```haskell
-- Calculate the time weighted average of the queue length
-- Stop when the teller starts serving the last customer

getAverageQueueLength' :: Double -> Double -> Double -> [(Double, Double)] -> Double
getAverageQueueLength' a t _ [] = a / t
getAverageQueueLength' a t1 l1 ((t2, l2) : xs) = getAverageQueueLength' (a + l1 * (t2 - t1)) t2 l2 xs

getAverageQueueLength :: [(Double, Double)] -> Double
getAverageQueueLength = getAverageQueueLength' 0 0 0
```


```haskell
-- For red customers, the average processing time is higher than the average arrival rate, so the queue will keep lengthening over time.
-- We will assume that the queue will be closed after 8 hours, but that the teller will work overtime to process all queued customers.
count = 60 * 60 * 8
arriveTimes = getArriveTimes (mkStdGen 1) count
processTimes = getProcessTimes (mkStdGen 2) Red $ length arriveTimes
(startTimes, _finishTimes) = unzip $ getFinishTimes arriveTimes processTimes
queueLengths = getQueueLengths arriveTimes startTimes

-- The maximum is easy
maximum $ map snd queueLengths
```


    1395.0



```haskell
-- And the average is now also easy with our helper above
getAverageQueueLength queueLengths
```


    698.4074261783019


## Q3: Which type of customer gives the gives the closest value between the average and maximum waiting times?

In other words, which type of customer result in a less severe outlier effect?

We have the numbers of yellow above, so we just need to do the same for blue and red to compare.


```haskell
yellowDifference = yellowMaximum - yellowAverage
yellowDifference
```


    44.12778983010999



```haskell
processTimes = getProcessTimes (mkStdGen 2) Blue $ length arriveTimes
(startTimes, _finishTimes) = unzip $ getFinishTimes arriveTimes processTimes
waitTimes = getWaitTimes arriveTimes startTimes
blueAverage = sum waitTimes / genericLength waitTimes
blueMaximum = maximum waitTimes
blueMaximum - blueAverage
```


    32828.15509014766



```haskell
processTimes = getProcessTimes (mkStdGen 2) Red $ length arriveTimes
(startTimes, _finishTimes) = unzip $ getFinishTimes arriveTimes processTimes
waitTimes = getWaitTimes arriveTimes startTimes
redAverage = sum waitTimes / genericLength waitTimes
redMaximum = maximum waitTimes
redMaximum - redAverage
```


    23077.450710208836


Blue and red are crazy :)
Yellow gives the absolute difference between average and maximum. Relative difference is left as an exercise for the reader.
